+++
title = "Week1 mini Project"
description = "Use Zola to develop a static site."
date = 2024-01-27T09:19:42+00:00
updated = 2024-01-27T09:19:42+00:00
draft = false
template = "portfolio/page.html"

[extra]
lead = "This is the readme of the <b>Zola Static Site</b> program."
+++

In this project, I create a static site with Zola, a Rust static site generator, that will hold all of the portfolio work in this class. 

after run 'zola serve', ths site is available at localhost:1111
